# DecemberUtilities

[![CI Status](https://img.shields.io/travis/alvaro@decemberlabs.com/DecemberUtilities.svg?style=flat)](https://travis-ci.org/alvaro@decemberlabs.com/DecemberUtilities)
[![Version](https://img.shields.io/cocoapods/v/DecemberUtilities.svg?style=flat)](https://cocoapods.org/pods/DecemberUtilities)
[![License](https://img.shields.io/cocoapods/l/DecemberUtilities.svg?style=flat)](https://cocoapods.org/pods/DecemberUtilities)
[![Platform](https://img.shields.io/cocoapods/p/DecemberUtilities.svg?style=flat)](https://cocoapods.org/pods/DecemberUtilities)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

DecemberUtilities is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'DecemberUtilities'
```

## Author

alvaro@decemberlabs.com, alvaro@decemberlabs.com

## License

DecemberUtilities is available under the MIT license. See the LICENSE file for more info.
