#
# Be sure to run `pod lib lint DecemberUtilities.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DecemberUtilities'
  s.version          = '1.0.1'
  s.summary          = 'December Utilities'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
December Utilities.
                       DESC

  s.homepage         = 'https://alrose21@bitbucket.org/alrose21/december-ios-utilities.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'alvaro@decemberlabs.com' => 'alvaro@decemberlabs.com' }
  s.source           = { :git => 'https://alrose21@bitbucket.org/alrose21/december-ios-utilities.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '10.0'
  s.ios.deployment_target = '11.0'
  s.swift_version = '5.0'
  s.source_files = 'DecemberUtilities/Classes/*'
  
  s.resource_bundles = {
    'DecemberUtilities' => ['DecemberUtilities/Classes/*.xib']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
