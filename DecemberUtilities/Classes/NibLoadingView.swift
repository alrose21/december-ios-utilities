//
//  NibLoadingView.swift
//  DecemberLabsUtilities
//
//  Created by Alvaro Rose on 7/4/18.
//  Copyright © 2018 Wavelet Health. All rights reserved.
//
//How To Use:
//Create a nib file where the name of the file matches the class name
//set the xib's files owner to the relevant class. Important! set files owner and not the UIView class on the xib object itself
//Using this approach you can still set iboutlets and properties as normal
import UIKit

open class NibLoadingView: UIView {
    
    var view: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        view = loadViewFromXib()
        view.frame = bounds
        view?.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromXib() -> UIView {
        
        let bundle = Bundle(for: type(of: self))
        
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        
        if let view = nib.instantiate(withOwner: self, options: nil)[0] as? UIView {
            return view
        }
        
        return UIView()
        
    }
    
}
