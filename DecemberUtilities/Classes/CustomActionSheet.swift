//
//  CustomActionSheet.swift
//  DecemberUtilities
//
//  Created by Alvaro Rose on 9/2/19.
//  Copyright © 2019 Wavelet Health. All rights reserved.
//

import UIKit

open class CustomActionSheet: UIView {
    @IBOutlet weak public var view: UIView!
    @IBOutlet weak public var backgroundView: UIView!
    @IBOutlet weak public var containerView: UIView!
    @IBOutlet weak public var containerTopToSuperviewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak public var draggableView: UIView!
    @IBOutlet weak public var topGrayView: UIView!
    @IBOutlet weak public var scrollView: UIScrollView!
    @IBOutlet weak public var customizableView: UIView!
    @IBOutlet weak public var customizableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak public var scrollViewToSafeareaBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak public var scrollViewHeightConstraint: NSLayoutConstraint!
    
    public var safeAreTopInset: CGFloat?
    public var safeAreBottomInset: CGFloat?
    public var panGesture: UIPanGestureRecognizer?
    public var tapGesture: UITapGestureRecognizer?
    public var childView: UIView?

    //set this value when you have a table on the childview
    public var childViewContentHeight: CGFloat? {
        didSet {
            setHeight()
        }
    }

    override public init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
  
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
  
    public func commonInit() {
        let podBundle = Bundle(for: CustomActionSheet.self)
        podBundle.loadNibNamed(String(describing: CustomActionSheet.self), owner: self, options: nil)
        self.alpha = 0
        if let view = view {
            self.addSubview(view)
            view.translatesAutoresizingMaskIntoConstraints = false
            self.addConstraint(NSLayoutConstraint(item: view, attribute: .top,
                                                relatedBy: .equal, toItem: self,
                                                attribute: .top, multiplier: 1, constant: 0))
            self.addConstraint(NSLayoutConstraint(item: view, attribute: .bottom,
                                                relatedBy: .equal, toItem: self, attribute: .bottom,
                                                multiplier: 1, constant: 0))
            self.addConstraint(NSLayoutConstraint(item: view, attribute: .left,
                                                relatedBy: .equal, toItem: self, attribute: .left,
                                                multiplier: 1, constant: 0))
            self.addConstraint(NSLayoutConstraint(item: view, attribute: .right,
                                                relatedBy: .equal, toItem: self, attribute: .right,
                                                multiplier: 1, constant: 0))
        }

        if let window = UIApplication.shared.windows.first {
            scrollViewToSafeareaBottomConstraint.constant = window.safeAreaInsets.bottom
            safeAreTopInset = window.safeAreaInsets.top
            safeAreBottomInset = window.safeAreaInsets.bottom
        }

        setUpUI()
        setUpGestures()
        updateGesturesState(enabled: true)
        loadCustomView()
    }
  
    open func loadCustomView() {
        if let childView = childView {
            customizableView.addSubview(childView)
            childView.translatesAutoresizingMaskIntoConstraints = false
            customizableView.addConstraint(NSLayoutConstraint(item: childView, attribute: .top, relatedBy: .equal, toItem: customizableView, attribute: .top, multiplier: 1, constant: 0))
            customizableView.addConstraint(NSLayoutConstraint(item: childView, attribute: .bottom, relatedBy: .equal, toItem: customizableView, attribute: .bottom, multiplier: 1, constant: 0))
            customizableView.addConstraint(NSLayoutConstraint(item: childView, attribute: .left, relatedBy: .equal, toItem: customizableView, attribute: .left, multiplier: 1, constant: 0))
            customizableView.addConstraint(NSLayoutConstraint(item: childView, attribute: .right, relatedBy: .equal, toItem: customizableView, attribute: .right, multiplier: 1, constant: 0))
        }
        setHeight()
    }

    func setHeight() {
        layoutIfNeeded()
        var containerTopMinimumPadding: CGFloat = 90
        if let safeAreTopInset = safeAreTopInset, let safeAreBottomInset = safeAreBottomInset {
            containerTopMinimumPadding += safeAreTopInset + safeAreBottomInset
        }
        let containerMaximumHeight = view.frame.height - containerTopMinimumPadding
        if let childViewContentHeight = childViewContentHeight {
            scrollView.isScrollEnabled = false
            customizableViewHeightConstraint.constant = min(childViewContentHeight, containerMaximumHeight)
            scrollViewHeightConstraint.constant = min(childViewContentHeight, containerMaximumHeight)
        } else {
            scrollView.isScrollEnabled = true
            scrollViewHeightConstraint.constant = min(customizableView.frame.height, containerMaximumHeight)
        }
        layoutIfNeeded()
    }

    func setUpUI() {
        containerView.layer.cornerRadius = 15.0
        containerView.layer.masksToBounds = true

        topGrayView.layer.cornerRadius = 3.0
        topGrayView.layer.masksToBounds = true
    }

    func setUpGestures() {
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture))
        draggableView.addGestureRecognizer(panGesture!)

        tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        backgroundView.addGestureRecognizer(tapGesture!)
    }

    @objc func handlePanGesture(_ gestureRecognizer: UIPanGestureRecognizer) {
        guard let gestureView = gestureRecognizer.view else { return }

        switch gestureRecognizer.state {
        case .began:
            break
        case .changed:
            let translation = gestureRecognizer.translation(in: gestureView)
            let height = containerView.frame.height
            if translation.y < 0 {
                containerTopToSuperviewBottomConstraint.constant = height
            } else {
                containerTopToSuperviewBottomConstraint.constant = height - translation.y
            }
            layoutIfNeeded()
        default:
            let minimumVerticalAcceptableValue: CGFloat = 50.0
            if containerView.frame.height - containerTopToSuperviewBottomConstraint.constant < minimumVerticalAcceptableValue {
                //despresiable scroll, show it
                show()
            } else {
                hide()
            }
        }
    }

    @objc func handleTapGesture(_ gestureRecognizer: UITapGestureRecognizer) {
        hide()
    }

    func updateGesturesState(enabled: Bool) {
        tapGesture?.isEnabled = enabled
        panGesture?.isEnabled = enabled
    }
    
    public func hide() {
        containerTopToSuperviewBottomConstraint.constant = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.layoutIfNeeded()
            self.alpha = 0
        }) { _ in
            self.removeFromSuperview()
        }
    }

    public func show() {
        containerView.layoutIfNeeded()
        containerTopToSuperviewBottomConstraint.constant = containerView.frame.height
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
            self.alpha = 1
        }
    }
}
