//
//  ViewController.swift
//  DecemberUtilities
//
//  Created by alvaro@decemberlabs.com on 01/24/2020.
//  Copyright (c) 2020 alvaro@decemberlabs.com. All rights reserved.
//

import UIKit
import DecemberUtilities

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let customActionSheet = CustomView(frame: view.frame)
        present(customActionSheet: customActionSheet)
    }

    func present(customActionSheet: CustomActionSheet) {
        customActionSheet.frame = view.frame
        view.addSubview(customActionSheet)
        let centerY = NSLayoutConstraint(item: view, attribute: .centerX, relatedBy: .equal, toItem: customActionSheet, attribute: .centerX, multiplier: 1, constant: 0)
        let centerX = NSLayoutConstraint(item: view, attribute: .centerY, relatedBy: .equal, toItem: customActionSheet, attribute: .centerY, multiplier: 1, constant: 0)
        view.addConstraints([centerX,centerY])
        customActionSheet.show()
    }
}

