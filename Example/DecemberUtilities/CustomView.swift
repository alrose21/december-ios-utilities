//
//  CustomView.swift
//  Biostrap
//
//  Created by Alvaro Rose on 8/7/19.
//  Copyright © 2019 Wavelet Health. All rights reserved.
//

import UIKit
import DecemberUtilities

class CustomView: CustomActionSheet {
    @IBOutlet var mainView: UIView!
    
    override func loadCustomView() {
        Bundle.main.loadNibNamed("CustomView", owner: self, options: nil)
        childView = mainView
        
        
        super.loadCustomView()
    }
    
    @IBAction func test(_ sender: Any) {
        print("test")
    }
    
}
